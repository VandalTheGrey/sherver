from impacket import smbserver

class Server(smbserver.SimpleSMBServer):
    def __init__(self, ip, port, share_dir, share_name='shells'):
        super(Server, self).__init__(listenAddress=ip, listenPort=port)
        
        self.addShare(share_name, share_dir, '')
        self.setSMB2Support(True)
        self.setSMBChallenge('')
        self.setLogFile('')


