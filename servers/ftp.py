import logging

from pyftpdlib.authorizers import DummyAuthorizer
from pyftpdlib.handlers import FTPHandler
from pyftpdlib.servers import ThreadedFTPServer

class Server(ThreadedFTPServer):
    def __init__(self, ip, port, directory, verbose=False):
        logger = logging.getLogger('pyftpdlib')
        if verbose:
            logger.setLevel(logging.INFO)
        else:
            logger.disabled = True

        authorizer = DummyAuthorizer()
        authorizer.add_anonymous(directory)

        handler = FTPHandler
        handler.authorizer = authorizer

        super(Server, self).__init__((ip, port), handler)

