from http.server import HTTPServer, SimpleHTTPRequestHandler

class Handler(SimpleHTTPRequestHandler):
    directory = None

    def __init__(self, *args, **kwargs):
        if self.directory:
            super().__init__(*args, directory=self.directory, **kwargs)
        else:
            super().__init__(*args, **kwargs)


