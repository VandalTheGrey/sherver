#!/usr/bin/env python3

import argparse
import jinja2
import logging
import os
import psutil
import re
import shutil
import socketserver
import sys
import time

from threading import Thread

import servers.ftp
import servers.http
import servers.smb

socketserver.TCPServer.allow_reuse_address = True

BANNER = """
    _____ __  ____________ _    ____________ 
   / ___// / / / ____/ __ \ |  / / ____/ __ \ 
   \__ \/ /_/ / __/ / /_/ / | / / __/ / /_/ /
  ___/ / __  / /___/ _, _/| |/ / /___/ _, _/ 
 /____/_/ /_/_____/_/ |_| |___/_____/_/ |_|  

                           -VandalTheGrey
                           """
                                                            
NOT_FOUND = 'N/A'
SERVER_DIR = '/tmp/sherver/'
SHELLS_DIR = os.path.join(SERVER_DIR, 'shells/')

lhost = ''
lport = ''
verbose = None

ftp_server = None
http_server = None
smb_server = None

def main():
    global lhost
    global lport
    global verbose

    print(BANNER)

    default_address = get_default_address()

    parser = argparse.ArgumentParser()
    parser.add_argument('-i', '--ip', action='store', default='0.0.0.0', help='IP Address to run the server on')
    if default_address == NOT_FOUND:
        parser.add_argument('-l', '--lhost', action='store', required=True, help='LHOST for reverse shells')
    else:
        parser.add_argument('-l', '--lhost', action='store', default=default_address, required=False, help=f'LHOST for reverse shells [Default: {default_address}]')
    parser.add_argument('-p', '--lport', action='store', default=4444, required=False, help='LPORT for reverse shells [Default: 4444]')
    parser.add_argument('-F', '--ftp', action='store_true', default=False, help='Enable FTP Server. If no servers specified, all will run')
    parser.add_argument('-H', '--http', action='store_true', help='Enables HTTP Server. If no servers specified, all will run')
    parser.add_argument('-S', '--smb', action='store_true', help='Enables SMB Server. If no server specified, all will run')
    parser.add_argument('--ftp-port', action='store', default=2121, required=False, metavar='PORT')
    parser.add_argument('--http-port', action='store', default=8000, required=False, metavar='PORT')
    parser.add_argument('--smb-port', action='store', default=445, required=False, metavar='PORT')
    parser.add_argument('-c', '--current', action='store_true', default=False, help='Add files from current directory (Non-Recurse)')
    parser.add_argument('-C', '--current-all', action='store_true', default=False, help='Add files from current directory (Recurse) WARNING: May copy MANY files')
    parser.add_argument('-v', '--verbose', action='store_true', default=False)

    try:
        options = parser.parse_args()
    except Exception as e:
        logging.critical(str(e))
        sys.exit(1)

    if not (options.ftp or options.http or options.smb):
        options.ftp = True
        options.http = True
        options.smb = True

    lhost = options.lhost
    lport = options.lport
    verbose = options.verbose

    print(f'LHOST:\t{options.lhost}')
    print(f'LPORT:\t{options.lport}')
    print()

    server_threads = []
    if options.ftp:
        print(f'FTP Server:\t{options.ip}:{options.ftp_port}')
        server_threads.append(Thread(target=start_ftp_server, args=(options.ip, options.ftp_port, SHELLS_DIR)))
    if options.http:
        print(f'HTTP Server:\t{options.ip}:{options.http_port}')
        server_threads.append(Thread(target=start_http_server, args=(options.ip, options.http_port, SHELLS_DIR)))
    if options.smb:
        print(f'SMB Server:\t{options.ip}:{options.smb_port}')
        server_threads.append(Thread(target=start_smb_server, args=(options.ip, options.smb_port, SHELLS_DIR)))

    clean()

    shell_paths = [os.path.join(os.path.dirname(os.path.realpath(__file__)), 'shells/'), '/etc/sherver/shells/', f'{os.path.expanduser("~")}/.local/sherver/shells/']
    for shell_path in shell_paths:
        if verbose:
            print(f"[-] Adding dir {shell_path}")
        add_files(shell_path, base=shell_path)

    if options.current_all:
        add_files(os.getcwd(), base=os.getcwd())
    elif options.current:
        add_files(os.getcwd(), False, os.getcwd())

    for t in server_threads:
        t.daemon = True
        t.start()

    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        pass

def get_default_address():
    addrs = psutil.net_if_addrs()
    if 'tun0' in addrs.keys():
        # The ipv4 address should be first
        # TODO: More robust searching
        return addrs['tun0'][0].address
    for addr in addrs:
        if addrs[addr][0].address != '127.0.0.1':
            return addrs[addr][0].address
    return NOT_FOUND

def clean():
    try:
        shutil.rmtree(SERVER_DIR)
    except FileNotFoundError:
        pass
    except Exception as e:
        print(e)
    os.mkdir(SERVER_DIR)

def add_files(dirpath, recurse=True, base=None):
    global verbose

    if not os.path.exists(dirpath):
        if verbose:
            print(f'[!] Directory not found: {dirpath}')
        return

    normalized_path = re.sub(base, '', dirpath)
    normalized_path = re.sub(r'^[\\\/]', '', normalized_path)
    os.makedirs(os.path.join(SHELLS_DIR, normalized_path), exist_ok=True)
    for item in os.listdir(dirpath):
        item_path = os.path.join(dirpath, item)
        if os.path.isfile(item_path):
            new_path = re.sub(base, '', item_path)
            if new_path.startswith('/'):
                new_path = new_path[1:]
            dst_path = os.path.join(SHELLS_DIR, new_path)
            if verbose:
                print(f'[+] Adding file: {item_path}')
            if item_path.endswith('.j2'):
                render_template(item_path, dst_path)
            else:
                shutil.copyfile(item_path, dst_path)
        else:
            if recurse:
                add_files(item_path, recurse, base)

def render_template(src_path, dst_path):
    global lhost
    global lport
    dst_path = re.sub(r'\.j2$', '', dst_path)
    with open(src_path) as f:
        template = jinja2.Template(f.read())
    with open(dst_path, 'w') as f:
        f.write(template.render(lhost=lhost, lport=lport))


def start_ftp_server(ip, port, directory):
    global ftp_server
    ftp_server = servers.ftp.Server(ip, port, directory)
    ftp_server.serve_forever()

def start_http_server(ip, port, directory):
    global http_server
    servers.http.Handler.directory = directory
    http_server = socketserver.TCPServer((ip, port), servers.http.Handler)
    http_server.serve_forever()

def start_smb_server(ip, port, directory):
    global smb_server
    smb_server = servers.smb.Server(ip, port, directory)
    smb_server.start()

if __name__ == "__main__":
    main()
